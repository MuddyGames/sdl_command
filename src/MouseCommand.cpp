#include <stdio.h>
#include <./include/MouseCommand.h>

MouseCommand::MouseCommand(const char letter) : m_letter(letter)
{
}

MouseCommand::~MouseCommand() {}

void MouseCommand::execute() {
    printf("Just Checking on Character: %c \n", m_letter );
}

void MouseCommand::undo() {
    printf("Just Undo-ing on Character: %c \n", m_letter );
}