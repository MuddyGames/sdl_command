#define SDL_MAIN_HANDLED
#include <./include/sample_header.h>
#include <./include/MouseCommand.h>

#include <SDL2/SDL.h>
#include <stdio.h>

#include <stack>

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

void mouseButtonDown(SDL_MouseButtonEvent &button_event);

// Put the commands in a Stack
std::stack<Command *> commands;

int main(int argc, char *argv[])
{
	// Is Game Running
	bool isGameRunning = true;

	// Handle arguments (Note C style)
	printf("argc = %d\n", argc);
	for (int i = 0; i < argc; ++i)
	{
		printf("argv[ %d ] = %s\n", i, argv[i]);
	}

	// Call custom methods
	printMakefileSuccess();

	//Render Window
	SDL_Window *window = NULL;

	//Frame or Surface to Render to
	SDL_Surface *frame = NULL;

	// Clean up SDL
	atexit(SDL_Quit);

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("Initialization Error::SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		//Create window
		window = SDL_CreateWindow("Visual Studio Code SDL Starter Kit (Windows 10 MSYS64 SDL2)", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			printf("Window Creation Error::SDL_Error: %s\n", SDL_GetError());
		}
		else
		{
			while (isGameRunning)
			{
				//Get Window Frame to Render to
				frame = SDL_GetWindowSurface(window);

				SDL_Event e;
				while (SDL_PollEvent(&e))
				{
					switch (e.type)
					{
					case SDL_QUIT:
						isGameRunning = false;
						exit(0);
						break;
					case SDL_MOUSEBUTTONDOWN:
						mouseButtonDown(e.button);
						break;
					}

					//Fill the Frame with Red
					SDL_FillRect(frame, NULL, SDL_MapRGB(frame->format, 0xFF, 0x00, 0x00));

					printf("Polling!\n");

					//Update the surface
					SDL_UpdateWindowSurface(window);
				}
			}
		}
	}

	return 0;
}

void mouseButtonDown(SDL_MouseButtonEvent &button_event)
{
	if (button_event.button == SDL_BUTTON_LEFT)
	{
		printf("Hello from the mouse!\n");

		commands.push(new MouseCommand('M'));
		commands.top()->execute();
		commands.push(new MouseCommand('o'));
		commands.top()->execute();
		commands.push(new MouseCommand('u'));
		commands.top()->execute();
		commands.push(new MouseCommand('s'));
		commands.top()->execute();
		commands.push(new MouseCommand('e'));
		commands.top()->execute();
	}
	else if (button_event.button == SDL_BUTTON_RIGHT)
	{
		if (!commands.empty())
		{
			commands.top()->undo();
			commands.pop();
		}
	}
}
