# README #

This project is a simple command pattern, used in Conjunction with SDL

## What is this repository for? ##
* Getting stated with Command Pattern

### How do I get set up? ###
* Download Visual Studio Code
* Install C/C++ for Visual Studio Code
> `ctrl+shift+x`
search for `@ext:ms-vscode.cpptools` or `C/C++ IntelliSense, debugging, and code browsing.`
Install this plugin,
* Ensure build tools are installed one linux run terminal command `sudo apt install build-essential gdb`. This installs essential build tools and gdb debugger. For Windows see [Using GCC with MinGW](https://code.visualstudio.com/docs/cpp/config-mingw).
* Create Makefiles and Source
* Run `make` from a MSYS64 Bash, click left and right mouse to see that a command or set of commands are executed.

## Useful Resources ##
* [Game Programming Patterns (Command Pattern)](https://gameprogrammingpatterns.com/command.html)

## Who do I talk to? ##
* philip.bourke@itcarlow.ie