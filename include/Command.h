#ifndef COMMAND_HEADER
#define COMMAND_HEADER

class Command
{
public:
	Command();
	virtual ~Command();
	virtual void execute() = 0;
	virtual void undo() = 0;
};
#endif