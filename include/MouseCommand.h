#ifndef MOUSE_COMMAND_HEADER
#define MOUSE_COMMAND_HEADER

#include <./include/Command.h>

class MouseCommand : public Command
{
public:
	MouseCommand(const char letter);
	virtual ~MouseCommand();

	void execute() override;
	void undo() override;

protected:
	const char m_letter;
};
#endif